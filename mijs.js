var msg1="Por favor verifique sus datos de acceso";
var msg2="Por favor ingresa los datos";
var msg3="Se envio un email con sus datos de acceso";
var msg4="Su usuario se registro satisfactoriamente se envio un email para su activación";
function salir(){
	localStorage.removeItem("usuario");
	window.location = "./index.html";
}
function validar_email( email ) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}
function login(u){
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	console.log(url);
	datos = {};
	var usuario ="usuario";
	$.getJSON(url, datos, function(data){
		if(data.status=="200"){
				localStorage.setItem(usuario,data.celular);
		    window.location = "./home.html";
		}else{
			$(".txterror").text(msg1);
			$(".msgerror").show();
			$(".msglogin").show("slow");
		}
	});
}
function invitado(){
	localStorage.setItem("usuario","31000000");
	localStorage.setItem("nombre","Invitado");
   window.location = "./home.html";
}
function loadDatos(u){
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	datos = {};
	$.getJSON(url, datos, function(data){
		if(data.status=="200" && data.estado=="0"){
		     $(".sombracontactupd").show("slow");
				 $(".dcodeupd").show("slow");
		}else if(data.status=="200" && data.estado=="1"){
				localStorage.setItem("usuario",data.celular);
				localStorage.setItem("nombre",data.nombre);
				localStorage.setItem("email",data.email);
				localStorage.setItem("direccion",data.direccion);
				localStorage.setItem("cumpleanos",data.cumpleanos);
				$(".headerperfil h2").text(data.nombre);
				if (data.email=="invitado@aipcolombia.co") {
					$(".headerperfil h3").text("");
				}else{
					$(".headerperfil h3").text(data.email);
				}

		}else{
			 window.location = "./inicio.html";
		}
	});
}
function dia(){
	url = 'https://www.aipcolombia.co/app/api/dia';
	datos = {};
	$.getJSON(url, datos, function(data){

			if(data.status=="200"){
        $(".recetadestacada h3").text(data.dia);
				$(".imgdestreceta").attr("src","http://aipcolombia.co/imgsapp/"+data.imagen);
        $(".titulorecetadias").text(data.nombre);
				$(".titulorecetadias").attr("id",data.id);
			}
	});
}
function cargarreceta(idr) {
	url = 'https://www.aipcolombia.co/app/api/receta/'+idr;
	datos = {};
	$.getJSON(url, datos, function(data){
		if(data[0].estado=="1"){
			//$(".recetadestacada h3").text(data[0].dia);
			var  detallereceta=""
			$(".detallereceta").empty("");
      $(".contentingredientes").empty("");
       $(".utensillos").empty("");

			$(".smimgdestacada").attr("src","http://aipcolombia.co/imgsapp/"+data[0].imgdestacada);
			$(".smtituloreceta").text(data[0].nombre);
      if (data[0].dificultad!="") {detallereceta+="<li>Dificultad: <label >"+data[0].dificultad+"</label></li>";}
			if (data[0].energia!="") {detallereceta+="<li>Energia: <label >"+data[0].energia+"</label></li>";}
			if (data[0].dificultad!="") {detallereceta+="<li>Calorías: <label >"+data[0].dificultad+"</label></li>";}
			if (data[0].grasa!="") {detallereceta+="<li>Grasa: <label >"+data[0].grasa+"</label></li>";}
			if (data[0].grasasat!="") {detallereceta+="<li>Grasa Saturada: <label >"+data[0].grasasat+"</label></li>";}
			if (data[0].carbohidratos!="") {detallereceta+="<li>Carbohidratos: <label >"+data[0].carbohidratos+"</label></li>";}
			if (data[0].azucar!="") {detallereceta+="<li>Azucar: <label >"+data[0].azucar+"</label></li>";}
			if (data[0].proteina!="") {detallereceta+="<li>Proteina: <label >"+data[0].proteina+"</label></li>";}
			if (data[0].colesterol!="") {detallereceta+="<li>Colesterol: <label >"+data[0].colesterol+"</label></li>";}
			if (data[0].sodio!="") {detallereceta+="<li>Sodio: <label >"+data[0].sodio+"</label></li>";}
			$(".detallereceta").html("<ul>"+detallereceta+"</ul>");
			$(".contentingredientes").html(data[0].ingredientes);
			$(".contentutensilios").html(data[0].utensillos);


		}else{

		}
	});
}
function	otrosDias() {
			$(".semanadias").empty();
			url = 'https://www.aipcolombia.co/app/api/otrosdias';
			datos = {};
			$.getJSON(url, datos, function(data){
				var categorias ="";
				if(data.length>=1){
					 for(var i = 0; i < data.length; i++) {
							categorias = '<div class="itemdias">'+
							    	        	'<div class="contitemdias relative">'+
							    	        		' <img src="https://aipcolombia.co/imgsapp/'+data[i].imagen+'">'+
							    	        		' <h3 id="'+data[i].id+'">'+data[i].dia+'</h3>'+
							    	        	'</div>'+
							             '</div>';
								$(".semanadias").append(categorias);
					 }
				}
			});
}
function	categoriasid(idc) {
			$(".sectioncategoriasid .contidcat").empty();
			url = 'https://www.aipcolombia.co/app/api/categoriasid/'+idc;
			datos = {};
			$.getJSON(url, datos, function(data){
				var categorias ="";
				if(data.length>=1){
					 for(var i = 0; i < data.length; i++) {
						 categorias = '<div class="fleft width50 itemcontidcat" id="abrirproducto" >'+
							                 '<div class=" width95 center item">'+
							                   '<img src="http://aipcolombia.co/imgsapp/'+data[i].imagen+'" >'+
							                   '<h2 id="'+data[i].id+'">'+data[i].nombre+'</h2>'+
																 '<label>$29.900</label>'
							                 '</div>'+
							             '</div>	';
								$(".sectioncategoriasid .contidcat").append(categorias);
					 }
				}
			});
			$(".sectiongeneral").hide('slow');
			$(".sectioncategoriasid").show('slow');
}

function alerta(noti){
	 switch (noti) {
	 	case 1:
			 $(".txterror").text("Por favor revise su email");
	 	break;
	  case 2:
			 $(".txterror").text("Por favor revise su nombre");
	  break;
		case 3:
		   $(".txterror").text("Por favor revise su número");
		break;
		case 4:
		   $(".txterror").text("El email no es valido");
		break;
		default:
	    $(".txterror").text("Fallo la conexión");
	 }
	 $(".msgerror").show();
	 $(".msglogin").show("slow");
}


function registro(n,e,u){
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	datos = {};
	$.getJSON(url, datos, function(data){
		if(data.status=="200"){
			$(".txterror").text("Usted ya se encuentra registrado");
			$(".msgerror").show();
			$(".msglogin").show("slow");
		}else{
				  var parametros = {
			        "nombre" : n,
			        "email" : e,
			        "celular" : u
			    };
	        $.ajax({
	                data:  parametros,
	                url:   'https://www.aipcolombia.co/app/api/registro/',
	                type:  'POST',
	                dataType: "json",
	                success:  function (data) {
												if(data.status=="200"){
													localStorage.setItem("usuario",data.celular);
													window.location = "./home.html";
												}else{
													alerta(0);
												}
	                	}
	         });
			 }
		});
}

function updperfil(n,e,d,c){
	var u = localStorage.getItem("usuario");
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	datos = {};
	$.getJSON(url, datos, function(data){
		if(data.status=="200"){
				  var parametros = {
			        "nombre" : n,
			        "email" : e,
							"direccion" : d,
							"cumpleanos" : c,
			        "usuario" : u
			    };
	        $.ajax({
	                data:  parametros,
	                url:   'https://www.aipcolombia.co/app/api/updperfil/',
	                type:  'POST',
	                dataType: "json",
	                success:  function (data) {
												if(data.status=="200"){
														localStorage.setItem("nombre",data.nombre);
														localStorage.setItem("email",data.email);
														localStorage.setItem("direccion",data.direccion);
														localStorage.setItem("cumpleanos",data.cumpleanos);
														$(".headerperfil h2").text(data.nombre);
														if (data.email=="invitado@aipcolombia.co") {
															$(".headerperfil h3").text("");
														}else{
															$(".headerperfil h3").text(data.email);
														}
													 $(".txterror").text("Perfil actualizado");
													 $(".msgerror").show();
													 $(".msglogin").show("slow");
												}else{
												  	$(".msgperfil").text("Intenta nuevamente");
														$(".msgerror").show();
														$(".msglogin").show("slow");
												}
	                	}
	         });
			 }
		});
}

function updpqrs(newpqrs){
	var u = localStorage.getItem("usuario");
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	datos = {};
	$.getJSON(url, datos, function(data){
		if(data.status=="200"){
				  var parametros = {
			        "texto" : newpqrs,
			        "usuario" : u
			    };
	        $.ajax({
	                data:  parametros,
	                url:   'https://www.aipcolombia.co/app/api/updpqrs/',
	                type:  'POST',
	                dataType: "json",
	                success:  function (data) {
												if(data.status=="200"){
													 $(".txterror").text("Gracias por su mensaje");
													 $(".msgerror").show();
													 $(".msglogin").show("slow");
													 $("#inputpqrs").val("");
												}else{
												  	$(".msgperfil").text("Intenta nuevamente");
														$(".msgerror").show();
														$(".msglogin").show("slow");
												}
	                	}
	         });
			 }
		});
}

function cargardatosu(u){
	url = 'https://www.aipcolombia.co/app/api/login/'+u;
	datos = {};
	$.getJSON(url, datos, function(data){

		if(data.status=="200"){
		 localStorage.setItem("usuario", data.celular);

		}else{
	        window.location = "./index.html";
		}
	});
}
function crearEvento(){
	if($(".cnevento").val()!="" && $(".cfechae").val()!=""){
        var evento = $(".cnevento").val();
        var fecha = $(".cfechae").val();
        var usuario = localStorage.getItem("usuario");
				var parametros = {
			        "evento" : evento,
			        "fecha" : fecha,
			        "usuario" : usuario
			    };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'https://www.aipcolombia.co/app/api/crearevento/', //archivo que recibe la peticion
                type:  'POST', //método de envio,
                dataType: "json",
                success:  function (data) { //una vez que el archivo recibe el request lo procesa y lo devuelve
												if(data.status=="200"){
											        $(".cnevento").val("");
											        $(".cfechae").val("");
							                        $(".formeventos").hide("slow");
							                         var itemevento='<div class="itemnoticia">'+
							                            '<img src="./img/pareja_03.jpg">'+
											              '<div class="left tleft">'+
											                '<h3>'+evento+'</h3>'+
											                '<p>'+fecha+'</p>'+
											                '<button id="'+data.id+'">Ver regalos</button>'+
											             ' </div>'+
											            '</div> ';
							                         $( ".itemsevents" ).append(itemevento);

												}else{

												     }
                 }
        });
	}
}
